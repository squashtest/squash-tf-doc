
Squash TF Execution Server 2.0.0 Release Note
=============================================


It replaces Squash TA Server 1.X.
Two goals in this version:

1) Easier installation :

* Creation of a docker image for the Execution Server
* Creation of an agent installer

2) Increase our execution capacity, especially with the creation of template for our two runners :

* A project template for the java junit runner
* A template for the cucumber java runner
* A template for execution in two passes
