
Squash TF Execution Server 2.1.1 & 2.1.0 Release Note
=====================================================

Major improvements in this release
----------------------------------

* Add docker images for execution agents :
  * Execution agent
  * Execution agent + X11 + firefox
  * Execution agent + X11 + Chrome
  
**Be careful** : Multi virtualization layering lead to error.  Trying to run the images on the “virtualization sandwich” Windows 10/Virtualbox/Debian 9/Docker results in an immediate crash of the container without any further ado.

* Upgrade jenkins to version 2.164.1 (lts). Jenkisn plugin have also been updated.



----

**> 2.1.1**

**Bug**

* SQTA-211 : The broken underscore escpaing behavior of htmlpublisher plugin is still active in the cucumber job.
* SQTA-210 : Template JUnit runner en mauvaise version


**> 2.1.0**

**Bug**

* SQTA-191 : Typo Doc Docker Agent install + info sur Win->VB->Linux->Docker
* SQTA-187 : L'installeur linux définit automatiquement un Git dont l'exécutable est git.exe
* SQTA-166 : Impossible d'exécuter des tests sur agent docker X11 (Essaye d'accéder à /home/agent)
* SQTA-153 : Nommage incorrect du Squash TF HTML Debug Report (HTML Publisher)
* SQTA-152 : Unstable test in the Galaxia plugin : depends on local repository history
* SQTA-132 : Agent docker X11 FF/GC crash après redémarrage
* SQTA-131 : Réparer le lien vers le fichier de conf d'installation headless dans la documentation
* SQTA-130 : Mot avec symboles dans installeur TF Jenkins Agent sur windows
* SQTA-129 : Création du dépot à un emplacement non souhaitable
* SQTA-128 : Problème pour l'exécution sur agent docker X11 (GlobalMavenSettings)
* SQTA-126 : Incohérence pour les GlobalMavenSettings lors de l'utilisation serveur Windows - agent Linux
* SQTA-112 : Pipelines inopérants sur le serveur Squash TF 2.1
* SQTA-110 : Stacktrace lors de l'accès au serveur TF dockerisé sur Debian
* SQTA-97 : Impossible de lancer l'agent docker + X11 + FF/Chrome sous Ubuntu
* SQTA-92 : Nommage incorrect du fichier "Test list" lors d'un list depuis le serveur
* SQTA-85 : Exécution programmée inutile
* SQTA-84 : [Branding] SQTF exec. server GUI & CLI installers not fully rebranded from TA to TF
* SQTA-68 : Plantage installeur TF Server sous Linux sans selection de Sahi



**Story**

* SQTA-40 : Mise à jour jenkins lts & plugin
* SQTA-27 : Agents TF 


