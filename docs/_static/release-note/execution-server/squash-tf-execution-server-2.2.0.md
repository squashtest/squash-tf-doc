
Squash TF Execution Server 2.2.0 Release Note
=====================================================



* Our docker images are published in dockerhub : https://hub.docker.com/u/squashtest
  
**Be careful** : Multi virtualization layering lead to error.  Trying to run the images on the “virtualization sandwich” Windows 10/Virtualbox/Debian 9/Docker results in an immediate crash of the container without any further ado.

* Job execution name are now customized according to the executed operation  

----

**Story**
* SQTA-241 : Plugin de nommage des build
* SQTA-236 : Modifier le système de publication des jobs template
* SQTA-102 : Mettre a jour le job template du runner cucumber avec le paramètre tf.feature

**Task**
* SQTA-59 : Publier nos images docker sur dockerhub
* SQTA-324 : Agent Linux : Personnalisation de la jdk dans l'installeur

**Bug**
* SQTA-328 : Agent Linux : Install FR Version : majorité des accents supprimés
* SQTA-317 : Agent Linux : Cumul des données dans parm.sh lors d'install avec écrasement
* SQTA-307 : Typo Doc - Doc sur agent docker
* SQTA-303 : Doc TF Server - TrOObleShooting
* SQTA-284 : L'nstalleur windows de l'agent laisse des variables "izpack" non résolues dans les scripts
* SQTA-283 : L'installeur de l'agent est en défaut sur un répertorie non vide
* SQTA-224 : Coquille dans la documentation de l'agent docker chrome
* SQTA-223 : Coquille dans la documentation du serveur d'exécution
* SQTA-199 : Typo Doc part 2 - Doc sur agent docker (+1 SQTF Exec Server)
* SQTA-190 : Date en UTC dans le container
* SQTA-425 : Agent windows : Le jdk à indiquer pendant l'installation non utilisé
* SQTA-422 : TM-TF w/ JUnit runner : Rapport envoyé est l'HTML Report et non le debug
* SQTA-379 : Install TF Server : Texte sur les licences
* SQTA-258 : A l'installation de l'exécution server on a des warning : "insufficient free space available after evicting expired cache entries"
* SQTA-257 : De vieux fichiers de log sont présent dans le serveur d'exécution.
* SQTA-127 : Présence de 2 workspace pour les agents TF
* SQTA-327 : Agent Linux : CSS affiché dans l'installeur en CLI

