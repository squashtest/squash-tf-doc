
Squash TF Execution Server 2.3.0 Release Note
=============================================

* Jenkins and Jenkins plugin update

  * **Be careful** this update lead us to modify our agent installation process.

* Adding Job template for our Robot Framework Runner

----

**Story**
* SQTA-461 : Créer le job template pour robot framework dans le serveur d'exécution
* SQTA-463 : Ecrire la doc utilisateur de l'utilisation des job basé sur les template
* SQTA-464 : Créer la doc utilisateur du job template et mettre à jour la doc du serveur
* SQTA-430 : Pouvoir définir le nom de l'agent au moment de sa création


**Task**
* SQTA-512 : Par homogénéité avec les autres jobs "linkTM-TF", positionner le job classique robotframework sur le SCM "aucun"
* SQTA-476 : Montée de versions de Jenkins et de ses plugins sur l'execution server
* SQTA-477 : Installation agent: la fonction remote CLI n'est plus supportée par Jenkins
* SQTA-488 : Mofifier l'image de config Jenkins dans l'installeur Izpack de l'agent suite au retrait de la fonction remote CLI
* SQTA-489 : Mettre à jour la doc de l'execution server et de l'agent suite à la montée de version de Jenkins et plugins
* SQTA-365 : Ecriture de nos job sous forme de pipeline / jenkinsFile

**Bug**
* SQTA-445 : Doc migration serveur : La migration des utilisateurs ne semble pas prise en compte
* SQTA-446 : Doc migration serveur : Aucune mention du configureTools maven
* SQTA-447 : Doc migration serveur : Workflow migration incorrect
* SQTA-212 : Using a test dependency to fetch the SKF db plugin used at runtime by the galaxia plugin tests breaks maven enforcer rules when SKF versions change.
* SQTA-531 : Dockerhub - FF/GC agent : Erreur sur le lien vers la doc d'installation 
* SQTA-374 : Agent Linux : "Test" dans la section Information lors de l'install
* SQTA-400 : Doc agent docker x11 : priviledged -> privileged
* SQTA-431 : Agent Linux : jdk indiquée non utilisée
* SQTA-513 : Renommer le template de job robotframework en lien TM/TA classique de manière cohérente avec les autres templates
* SQTA-518 : Le template de job cucumber échoue parfois à cause de fichiers restants d'exécutions précédentes
