
Squash TF Execution Server 2.3.1 Release Note
=============================================

* Disable Apache JServ Protocol (AJP) in Squash TF Execution Server configuration in response to Apache Tomcat vulnerability named Ghostcat (CVE-2020-1938)

----

**Task**
* SQUASH-410 : Disable Apache JServ Protocol (AJP) in Squash TF Execution Server configuration

