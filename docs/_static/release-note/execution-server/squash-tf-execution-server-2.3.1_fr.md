
Note de version du serveur d'exécution Squash TF 2.3.1
=============================================

* Désactivation de AJP (Apache JServ Protocol) dans la configuration du serveur d'exécution Squash TF en réponse à la faille de sécurité Ghostcat (CVE-2020-1938) de Apache Tomcat.

----

**Tâche**
* SQUASH-410 : Désactivation de AJP (Apache JServ Protocol) dans la configuration du serveur d'exécution Squash TF

