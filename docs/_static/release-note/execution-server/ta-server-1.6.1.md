Squash TA server - v1.6.1-RELEASE
====================================
2845    [TA] Enhancement        [Squash-TA Server] Include "Server" and the version in the default installation path and shortcut group name
3165    [TA] Enhancement        [TA server] Update sahi version
3181    [TA] Enhancement        [TA Server] Shortcut group for "Squash-TA-Server"
3158    [TA] Task	            [TA server] Update license header