Squash TA - srv-1.7.1-RELEASE
=============================
- 0006061: [[TA] Enhancement] Change build configuration of embedded jenkins for log4j 2.5 compatibility (kdrifi) - resolved.
- 0006057: [[TA] Enhancement] Patch maven embedded version 3.3.3 for srv. (kdrifi) - resolved.
- 0005938: [[TA] Enhancement] Updating java version : 1.6 -> 1.7 of Squash-TA-Server (kdrifi) - resolved.
- 0005846: [[TA] Enhancement] Upgrade Tomcat version 6.0.35 -> 8.0.30 (kdrifi) - resolved.
- 0005845: [[TA] Enhancement] Upgrade Sahi version 4.4 -> 5.0 (kdrifi) - resolved.
- 0005844: [[TA] Enhancement] Upgrade maven version 3.0.4 -> 3.3.3 (kdrifi) - resolved.
