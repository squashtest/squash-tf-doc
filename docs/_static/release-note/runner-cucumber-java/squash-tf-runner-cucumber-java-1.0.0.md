
Squash TF Runner Cucumber Java 1.0.0 Release Note
=================================================


This runner allows you to execute your tests (.feature) written in gherkin and their implementation written in cucumber
java inside our ecosystem.
Especially it's complete the gherkin feature of our brother project Squash TM and allows to launch the execution of tests
written in gherkin from squash TM

It's the first iteration of this runner. You can find the doc here : https://squash-tf.readthedocs.io/projects/runner-cucumber-java