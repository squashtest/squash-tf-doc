
Squash TF Runner Cucumber Java 1.1.0 Release Note
=================================================

Major improvements in this release
----------------------------------

* Add a new report dedicated to Cucumber execution result  

----

**Bug**
* SQTA-107 : Mots clés Gherkin non gérés par le runner cucumber => incohérence sur les status et scénarios affichés
* SQTA-145 : Remplacer "Click"
* SQTA-124 : Erreur de nommage dans les rapports fwk/junit/cucumber
* SQTA-171 : Le cleanup du mojo cucumber supprime tout le répertoire temporaire Squash TA
* SQTA-83 :  MojoFailedExecution: répétition de l'affichage des tests en erreur (BUILD FAILURE)
* SQTA-58 : [DOC] Version snapshot référencée pour l'archetype du runner cucumber-java
* SQTA-77 : Le runner Cucumber fournit un message d'erreur trop imprécis en cas de soucis avec le Json
* SQTA-72 : Runner Cucumber - Contrainte sur l'emplacement des sources compilés
* SQTA-76 : Runner Cucumber - Problème sur l'encodage des sources en UTF-8
* SQTA-82 : [Runner Gherkin] le répertoire "test-tree" est généré au mauvais endroit
* SQTA-81 : [Runner Gherkin] Warning intempestif
* SQTA-74 : Runner Cucumber - Inclusion des dépendances dans le plugin

**Story**
* SQTA-33 : [Runner Gherkin] Amélioration des rapports
