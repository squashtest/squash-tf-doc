
Squash TF Runner Cucumber Java 1.2.0 Release Note
=================================================

Major improvements in this release
----------------------------------

* Use a relative path for tf.feature ( ta.feature ) command line parameter (relative to the root of the project)

----

**Story**

* SQTA-101 : Nouveau paramètre tf.feature
* SQTA-100 : Pouvoir remplir le param ta.feature avec un chemin relatif

**Bug**

* SQTA-392 : Cucumber : Génère des rapports différents selon l'option tf.feature (relatif != absolu)
* SQTA-329 : HtmlGherkinSuiteResultExporter : Exceptions qui sortent du cadre (FF only)
* SQTA-235 : Missing parameter check in the cucumber runner leads to NullPointerException.
* SQTA-213 : The cucumber runner uses a prveious version of the squash-t(a|f) parent pom that does not properly ignores sphinx generated files when checking license headers.
* SQTA-168 : Le parameter ta.debug.mode n'est pas pris en compte  (Cucumber runner)
* SQTA-407 : Rapport Cucumber : Titre décalé
* SQTA-406 : Rapport Cucumber - Titre : Scrolling (GC), dépasse du reste du rapport (FF)
* SQTA-405 : Rapport Cucumber : Absence du logo Squash sur Chrome, 
