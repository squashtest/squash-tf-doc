

Squash TF Runner Java Junit 1.0.0 Release Note
==============================================


This runner allows you to execute your tests written in java junit inside our ecosystem in an almost transparent way
Especially it offer the possibility to launch the execution of your test written in java junit from our brother project Squash TM

It's the firt iteration of this runner. You can find the doc here : https://squash-tf.readthedocs.io/projects/runner-java-junit
