

Squash TF Runner Java Junit 1.0.3 Release Note
==============================================

**Bug**
* SQTA-108 : Nom d'artefact incorrect pour l'archetype du runner junit
* SQTA-69 : Runner Java JUnit - Problème avec les injections de bean Spring
* SQTA-55 : Incompatibilité du runner Java Junit avec maven 3.6.0
* SQTA-125 : Rapport JUnit runner - Manque d'infos (id, version, ...)
* SQTA-124 : Erreur de nommage dans les rapports fwk/junit/cucumber
* SQTA-200 : Le problème de cleanup détecté sur cucumber touche aussi junit-runner
* SQTA-57 : Les archetype des runner ne sont pas accessible dans notre dépôts maven
* SQTA-51 : Erreur ClassNotFound en lançant Selenium tests sur le Runner avec les classes dans des packages différents

**Bug**
* SQTA-13 : Intégration SOAP UI Java