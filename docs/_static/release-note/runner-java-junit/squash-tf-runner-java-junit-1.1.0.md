

Squash TF Runner Java Junit 1.1.0 Release Note
==============================================

Major improvements in this release
----------------------------------

* Add the metadata feature (which handle the Squash TM test case - automated test autolink)

-------------------------

**Story**
* SQTA-276 : Runner Junit : Valeur de "tf.metadata.check" entre crochet
* SQTA-278 : Runner Junit : Valeur de "tf.metadata.check.keys" entre crochet
* SQTA-311 : Evolution du rapport contenant les metadata du goal list du runner Junit
* SQTA-245 : Analyse d'unicité des valeurs des metadonnées restreinte à certaines clés
* SQTA-234 : Ajouter le goal check-metadata for Java JUnit Runner
* SQTA-233 : All TFMetadata annotation keys of a test method must be unique
* SQTA-225 : Modifier le goal RUN du runner junit pour vérifier les metadonnées
* SQTA-174 : Vérifier l'unicité de la valeur d'une métadonée pour une clé donnée sur le projet
* SQTA-172 : Modifier le goal list du runner junit pour ajouter les metadonnées
* SQTA-165 : Créer une annotation java pour le support des metadata TF dans le runner java junit

**Bug**
* SQTA-420 : JUnit list w/ metadata : Toujours en succès et pas de metadata dans le .json
* SQTA-320 : Rapport JUnit : Titre décalé
* SQTA-319 : Rapport JUnit : Logo Squash height : 0px sur Chrome
* SQTA-286 : Metadata jUnit - Répétition du message d'analyse complétée lors d'un build success
* SQTA-282 : Anomalie du rapport détaillé sur junit (et de l'exécution ?)
* SQTA-255 : Metadata jUnit - Clé à valeur multiples : Les éléments vides après la virgule sont acceptés lors du list
* SQTA-253 : ta.debug.mode non pris en compte (jUnit runner)
* SQTA-252 : Erreur dans le cas d'une metadata seule (junit) - TFMetadata is missing a default value for the element 'value'
* SQTA-216 : Fichier .properties généré en dehors du dossier Temp Squash_TA lors de l'execution du runner JUnit
* SQTA-144 : Rapport JUnit Runner - Test steps qui sortent du cadre
* SQTA-358 : JUnit - list : Error dans les logs : java.lang.ClassNotFoundException: org.squashtest.ta.galaxia.squash.tf.galaxia.annotations.TFMetadata
