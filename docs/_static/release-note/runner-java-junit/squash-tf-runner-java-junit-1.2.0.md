

Squash TF Runner Java Junit 1.2.0 Release Note
==============================================

Major improvements in this release
----------------------------------

* New feature the Squash TF Junit Runner is now able to retrieve parameter from TM 

-------------------------

**Story**
* SQTA-246 : Mécanisme de récupération des paramètres du testList.json dans les test java Junit 
* SQTA-323 : Créer la documentation des metadata avec le runner junit

**Bug**
* SQTA-528	Robot & JUnit - check-metadata : tf.metadata.check.keys non fonctionnel
* SQTA-530	JUnit - TM-TF + params : NPE aléatoire

