

Squash TF Runner Robot Framework 1.0.0 Release Note
===================================================


This runner allows you to execute your tests written in Rbot Framework inside our ecosystem in an almost transparent way.
Especially it offer the possibility to launch the execution of your test written in Robot Framework from our brother project Squash TM.
It comes with the autolink feature : link directly a TM tests case with a robot automation test using metadata (using Robot Framework tags). 
It could also retrieve parameters from TM.

It's the first iteration of this runner. You can find the doc here : https://squash-tf.readthedocs.io/projects/runner-robotframework
