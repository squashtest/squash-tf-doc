..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



###################
Squash TF Community
###################

.. toctree::
   :maxdepth: 1

   contribute.rst
   developer-guide.rst

.. note:: This part of the site is currently under construction

* Our bugtracker : `<https://ci.squashtest.org/mantis>`_. In this bugtracker, we have 3 projects wherein you could declare the bug :

  * Squash TF Execution Server
  * Squash TF Runners (for all the runners)

* Our forum : `<https://forum.squashtest.com/>`_
