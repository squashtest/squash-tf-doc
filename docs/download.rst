..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

################################
Squash TF Products Download Page
################################

.. _execution_server_download:

**************************
Squash TF Execution Server
**************************

**Latest Version : 2.3.1**

Squash TF Execution Server 2.3.1 Release Note (:download:`EN <_static/release-note/execution-server/squash-tf-execution-server-2.3.1_en.md>` / :download:`FR <_static/release-note/execution-server/squash-tf-execution-server-2.3.1_fr.md>`)

> Execution Server : Installer
==============================

* Linux : `squash-tf-execution-server-2.3.1-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.3.1-RELEASE-linux-installer.jar>`_

* Windows : `squash-tf-execution-server-2.3.1-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.3.1-RELEASE-win64-installer.jar>`_

Download previous version: :ref:`here <execution_server_installer_full>`

> Execution Server : Docker
===========================

The latest release tag is 2.3.1-RELEASE

Our images are now available on dockerhub :

* `<https://hub.docker.com/r/squashtest/squash-tf-execution-server>`_.

> Execution agent : Installer
=============================

* Linux : `squash-tf-execution-agent-2.3.1-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.3.1-RELEASE-linux-installer.jar>`_

* Windows : `squash-tf-execution-agent-2.3.1-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.3.1-RELEASE-win64-installer.jar>`_

Download previous version:  :ref:`here <execution_agent_installer_full>`

> Execution agent : Docker
==========================

The latest release tag is 2.3.1-RELEASE

Our images are now available on dockerhub :

* execution agent : `<https://hub.docker.com/r/squashtest/squash-tf-execution-agent>`_
* execution agent + X11 + chrome : `<https://hub.docker.com/r/squashtest/squash-tf-chrome-ui-execution-agent>`_
* execution agent + X11 + firefox : `<https://hub.docker.com/r/squashtest/squash-tf-firefox-ui-execution-agent>`_

-----------------------

*****************
Squash TF Runners
*****************

> Java Junit runner
===================

**Latest Version : 1.2.0**

* :download:`Squash TF Java Junit Runner 1.2.0 Release Note <_static/release-note/runner-java-junit/squash-tf-runner-java-junit-1.2.0.md>`


> Cucumber Java Runner
======================

**Latest Version : 1.2.0**

* :download:`Squash TF Cucumber Java Runner 1.2.0 Release Note <_static/release-note/runner-cucumber-java/squash-tf-runner-cucumber-java-1.2.0.md>`

> Robot Framework Runner
========================

**Latest Version : 1.0.0**

* :download:`Squash TF Robot Framework Runner 1.0.0 Release Note <_static/release-note/runner-robotframework/squash-tf-runner-robotframework-1.0.0.md>`

------------------





********
Archives
********

:ref:`Archives <tf-archives>`
