..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _squash_tf_doc_home:

##############################################
Welcome to Squash TF components documentation!
##############################################

.. toctree::
   :maxdepth: 2
   :hidden:

   < Squash TF Doc Portal > <https://squash-tf-portal.readthedocs.io>
   Execution Server <execution-server/execution-server.rst>
   Runners <runner/overview.rst>
   Download <download.rst>
   Roadmap <roadmap.rst>
   Community <developer/community.rst>


**Squash T**\ est **F**\ actory (aka **Squash TF**) is a set of components whose purpose is to help you run your project's
automated tests related to **Squash TM** (**Squash T**\ est **M**\ anagement) test cases.

Squash TF provide :

* Runners : They manage the link between Squash TM test cases and the automated tests
* An execution server : To handle the execution of automated tests drove by our runners

---------------------------

*******
Runners
*******

The Squash TF runners should be able to

* manage the test suites to execute
* launch the tests executions
* manage the reports creation
* when linked with Squash TM

  * report tests execution status to Squash TM
  * send execution reports URL to Squash TM

* list all the automated tests of the project

For now, the following runners are available :

* `Squash TF Java Junit Runner <./../../projects/runner-java-junit>`_
* `Squash TF Cucumber Java Runner <./../../projects/runner-cucumber-java>`_
* `Squash TF Robot Framework Runner <./../../projects/runner-robotframework>`_
* Squash TF UFT Runner (Commercial Runner)
* Squash TF Ranorex Runner (Commercial Runner)

Some other runners will come. The final aim is : Whatever technology you choose to implement your automated tests, we want to be able to execute them in Squash TF Execution Server.

---------------------------

****************
Execution Server
****************

The execution server is based on jenkins and its distributed build capabilities (for multi environments execution).
Use Jenkins bring us :

* all the features of the jenkins ecosystem ( scm connectors, master/agent mode, pipeline, ...)
* facilitate the integration in CI/CD pipelines

We :ref:`distribute <execution_server_download>` Squash TF Execution Server in two ways:

* as installer for windows and linux
* as docker image for linux
