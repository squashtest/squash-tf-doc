..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _tf-archives:

:orphan:

###########################
Squash TF Products archives
###########################

.. contents::
   :local:
   :backlinks: top

**************************
Squash TF Execution Server
**************************



> Execution Server : Installer
==============================

.. _execution_server_installer_full:

* **2.3.1** ``latest``

  * Linux : `squash-tf-execution-server-2.3.1-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.3.1-RELEASE-linux-installer.jar>`_

  * Windows : `squash-tf-execution-server-2.3.1-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.3.1-RELEASE-win64-installer.jar>`_


Previous Versions :

* 2.3.0

  * Linux : `squash-tf-execution-server-2.3.0-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.3.0-RELEASE-linux-installer.jar>`_

  * Windows : `squash-tf-execution-server-2.3.0-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.3.0-RELEASE-win64-installer.jar>`_

* 2.2.0

  * Linux : `squash-tf-execution-server-2.2.0-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.2.0-RELEASE-linux-installer.jar>`_

  * Windows : `squash-tf-execution-server-2.2.0-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.2.0-RELEASE-win64-installer.jar>`_

* 2.1.1

  * Linux : `squash-tf-execution-server-2.1.1-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.1.1-RELEASE-linux-installer.jar>`_

  * Windows : `squash-tf-execution-server-2.1.1-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.1.1-RELEASE-win64-installer.jar>`_

* 2.0.0

  * Linux : `squash-tf-execution-server-2.0.0-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.0.0-RELEASE-linux-installer.jar>`_

  * Windows : `squash-tf-execution-server-2.0.0-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-server-2.0.0-RELEASE-win64-installer.jar>`_



> Execution Agent : Installer
=============================

.. _execution_agent_installer_full:

* **2.3.1** ``latest``

  * Linux: `squash-tf-execution-agent-2.3.1-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.3.1-RELEASE-linux-installer.jar>`_

  * Windows: `squash-tf-execution-agent-2.3.1-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.3.1-RELEASE-win64-installer.jar>`_

Previous Versions :

* 2.3.0

  * Linux: `squash-tf-execution-agent-2.3.0-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.3.0-RELEASE-linux-installer.jar>`_

  * Windows: `squash-tf-execution-agent-2.3.0-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.3.0-RELEASE-win64-installer.jar>`_

* 2.2.0

  * Linux: `squash-tf-execution-agent-2.2.0-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.2.0-RELEASE-linux-installer.jar>`_

  * Windows: `squash-tf-execution-agent-2.2.0-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.2.0-RELEASE-win64-installer.jar>`_

* 2.1.1

  * Linux: `squash-tf-execution-agent-2.1.1-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.1.1-RELEASE-linux-installer.jar>`_

  * Windows: `squash-tf-execution-agent-2.1.1-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.1.1-RELEASE-win64-installer.jar>`_

* 2.0.0

  * Linux: `squash-tf-execution-agent-2.0.0-RELEASE-linux-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.0.0-RELEASE-linux-installer.jar>`_

  * Windows: `squash-tf-execution-agent-2.0.0-RELEASE-win64-installer.jar <http://repo.squashtest.org/distribution/squash-tf-execution-agent-2.0.0-RELEASE-win64-installer.jar>`_


> Docker
===========================

.. _execution_server_docker_full:


Our docker images are now published on dockerhub, you can pulled them directly from there: `<https://hub.docker.com/r/squashtest/squash-tf-execution-server>`_.

Previous Versions still available as tarball:

* 2.3.0-RELEASE : `squash-tf-execution-server.docker.2.3.0-RELEASE.tar <http://repo.squashtest.org/distribution/squash-tf-execution-server.docker.2.3.0-RELEASE.tar>`_

* 2.2.0-RELEASE : `squash-tf-execution-server.docker.2.2.0-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-execution-server.docker.2.2.0-RELEASE.tar>`_

* 2.1.1-release: `squash-tf-execution-server.docker.2.1.1-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-execution-server.docker.2.1.1-RELEASE.tar>`_

* 2.0.0-release: `squash-tf-execution-server.docker.2.0.0-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-execution-server.docker.2.0.0-RELEASE.tar>`_


> Execution agent : Docker
==========================

.. _execution_agent_docker_full:

Our docker images are now published on dockerhub, you can pulled them directly from there:

  * execution agent (linux): `<https://hub.docker.com/r/squashtest/squash-tf-execution-agent>`_

  * execution agent + X11 + chrome (linux): `<https://hub.docker.com/r/squashtest/squash-tf-chrome-ui-execution-agent>`_

  * execution agent + X11 + firefox (linux): `<https://hub.docker.com/r/squashtest/squash-tf-firefox-ui-execution-agent>`_

Previous version still available as tarball:

  * execution agent (linux):

    * 2.3.0-RELEASE: `squash-tf-execution-agent.docker.2.3.0-RELEASE.tar <http://repo.squashtest.org/distribution/squash-tf-execution-agent.docker.2.3.0-RELEASE.tar>`_
    * 2.2.0-RELEASE: `squash-tf-execution-agent.docker.2.2.0-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-execution-agent.docker.2.2.0-RELEASE.tar>`_
    * 2.1.1-RELEASE: `squash-tf-execution-agent.docker.2.1.1-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-execution-agent.docker.2.1.1-RELEASE.tar>`_

  * execution agent + X11 + chrome (linux):

    * 2.3.0-RELEASE: `squash-tf-chrome-ui-execution-agent.docker.2.3.0-latest.tar <http://repo.squashtest.org/distribution/squash-tf-chrome-ui-execution-agent.docker.2.3.0-RELEASE.tar>`_
    * 2.2.0-RELEASE: `squash-tf-chrome-ui-execution-agent.docker.2.2.0-latest.tar <http://repo.squashtest.org/acceptance/squash-tf-chrome-ui-execution-agent.docker.2.2.0-RELEASE.tar>`_
    * 2.1.1-RELEASE: `squash-tf-chrome-ui-execution-agent.docker.2.1.1-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-chrome-ui-execution-agent.docker.2.1.1-RELEASE.tar>`_

  * execution agent + X11 + firefox (linux):

    * 2.3.0-RELEASE: `squash-tf-firefox-ui-execution-agent.docker.2.3.0-RELEASE.tar <http://repo.squashtest.org/distribution/squash-tf-firefox-ui-execution-agent.docker.2.3.0-RELEASE.tar>`_
    * 2.2.0-RELEASE: `squash-tf-firefox-ui-execution-agent.docker.2.2.0-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-firefox-ui-execution-agent.docker.2.2.0-RELEASE.tar>`_
    * 2.1.1-RELEASE: `squash-tf-firefox-ui-execution-agent.docker.2.1.1-RELEASE.tar <http://repo.squashtest.org/acceptance/squash-tf-firefox-ui-execution-agent.docker.2.1.1-RELEASE.tar>`_

> Execution Server Release Note
===============================

* **2.3.1** ``latest``

  * Squash TF Execution Server 2.3.1 Release Note (:download:`EN <_static/release-note/execution-server/squash-tf-execution-server-2.3.1_en.md>` / :download:`FR <_static/release-note/execution-server/squash-tf-execution-server-2.3.1_fr.md>`)

Previous Versions :

* 2.3.0

  * :download:`Squash TF Execution Server 2.3.0 Release Note <_static/release-note/execution-server/squash-tf-execution-server-2.3.0.md>`

* 2.2.0

  * :download:`Squash TF Execution Server 2.2.0 Release Note <_static/release-note/execution-server/squash-tf-execution-server-2.2.0.md>`

* 2.1.1

  * :download:`Squash TF Execution Server 2.1.1 & 2.1.0 Release Note <_static/release-note/execution-server/squash-tf-execution-server-2.1.1.md>`

* 2.0.0

  * :download:`Squash TF Execution Server 2.0.0 Release Note <_static/release-note/execution-server/squash-tf-execution-server-2.0.0.md>`


-----------------------

******************
Squash TF Runner's
******************

> Java Junit runner
===================

**Latest Version : 1.2.0**

* :download:`Squash TF Java Junit Runner 1.2.0 Release Note <_static/release-note/runner-java-junit/squash-tf-runner-java-junit-1.2.0.md>`

Previous Versions :

* 1.1.0

  * :download:`Squash TF Java Junit Runner 1.1.0 Release Note <_static/release-note/runner-java-junit/squash-tf-runner-java-junit-1.1.0.md>`

* 1.0.3

  * :download:`Squash TF Java Junit Runner 1.0.3 Release Note <_static/release-note/runner-java-junit/squash-tf-runner-java-junit-1.0.3.md>`

* 1.0.0

  * :download:`Squash TF Java Junit Runner 1.0.0 Release Note <_static/release-note/runner-java-junit/squash-tf-runner-java-junit-1.0.0.md>`

> Cucumber Java Runner
======================

**Latest Version : 1.2.0**

* :download:`Squash TF Cucumber Java Runner 1.2.0 Release Note <_static/release-note/runner-cucumber-java/squash-tf-runner-cucumber-java-1.2.0.md>`

Previous Versions :

* 1.1.0

  * :download:`Squash TF Cucumber Java Runner 1.1.0 Release Note <_static/release-note/runner-cucumber-java/squash-tf-runner-cucumber-java-1.1.0.md>`

* 1.0.0

  * :download:`Squash TF Cucumber Java Runner 1.0.0 Release Note <_static/release-note/runner-cucumber-java/squash-tf-runner-cucumber-java-1.0.0.md>`


> Robot Framework Runner
========================

**Latest Version : 1.0.0**

* :download:`Squash TF Cucumber Java Runner 1.0.0 Release Note <_static/release-note/runner-robotframework/squash-tf-runner-robotframework-1.0.0.md>`


------------------

*******************
Archive Repository
*******************

For all bundles not listed here, you should found them in our `Archive repository <http://repo.squashtest.org/distribution/archives/>`__
