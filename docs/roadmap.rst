..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _roadmap:

#################
Squash TF Roadmap
#################

.. title:: Roadmap

*******
Runners
*******
Currently our runners concerns the Java platform (Java Junit Runner, Cucumber Java Runner) and Python (Robot Framework Runner).
We planned to support automated tests developed in other languages.

In our scope we have :

* Be able to launch an execution order from a Jenkins job with a Squash TM iteration identifier as parameter to retrieve the test list.
* Open the range of supported programming language. Python and C# currently are in our scope.
* Be able in the future to run tests written with studios like agilitest or Katalon.


**************************
Squash TF Execution Server
**************************

* We've planned to create a Jenkins plugin to provide a more user friendly way to connect Squash TF with Squash TM.
