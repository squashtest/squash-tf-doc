..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _runners:

#################
Squash TF Runners
#################

.. toctree::

   Java Junit Runner <https://squash-tf.readthedocs.io/projects/runner-java-junit/>
   Cucumber Java Runner <https://squash-tf.readthedocs.io/projects/runner-cucumber-java/>
   Robot Framework Runner <https://squash-tf.readthedocs.io/projects/runner-robotframework/>

*************************
Environment configuration
*************************

.. Hint:: Our runners are build to run in our Execution Server. So it's highly recommended to configure your development
          environment as describe in the section below.

-------------

First we advise to use the same tools versions as those we used in our execution server :

* maven: 3.5.0
* java: 1.8

-------------

Our maven library are hosted on our own repository. In consequence, you have to define our repository in your maven settings.
To do so, edit (or create) the maven ``settings.xml`` file in your ``.m2`` directory (The .m2 directory is generally
located in your Home directory) and add a new profile:

.. code-block:: xml

    <settings>

    ...

      <profiles>
        <profile>
          <id>tf-maven-repos</id>
          <!-- Squash TF maven repository -->
          <repositories>
            <repository>
              <id>org.squashtest.tf.release</id>
              <name>squashtest test factory - releases</name>
              <url>http://repo.squashtest.org/maven2/releases</url>
            </repository>
          </repositories>

          <!-- Squash TF maven plugin repository -->
          <pluginRepositories>
            <pluginRepository>
              <id>org.squashtest.plugins.release</id>
              <name>squashtest.org</name>
              <url>http://repo.squashtest.org/maven2/releases</url>
              <snapshots>
                <enabled>false</enabled>
              </snapshots>
              <releases>
                <enabled>true</enabled>
              </releases>
            </pluginRepository>
          </pluginRepositories>
        </profile>
      </profiles>

      <activeProfiles>
        <activeProfile>tf-maven-repos</activeProfile>
      </activeProfiles>

    </settings>

---------------

We also advise to patch your maven by using the procedure below for a better logging with our runners :

.. Note:: In all the procedure ``$MVN_HOME`` is your maven installation directory, and ``$MVN_VERSION`` your maven version.

* Add in ``$MVN_HOME/lib/ext/`` the jars:

  * `log4j-slf4j-impl-2.5.jar <http://central.maven.org/maven2/org/apache/logging/log4j/log4j-slf4j-impl/2.5/log4j-slf4j-impl-2.5.jar>`_
  * `log4j-core2.5.jar <http://central.maven.org/maven2/org/apache/logging/log4j/log4j-core/2.5/log4j-core-2.5.jar>`_
  * `log4j-api-2.5.jar <http://central.maven.org/maven2/org/apache/logging/log4j/log4j-api/2.5/log4j-api-2.5.jar>`_


- Create a logging configuration file called ``log4j2.xml`` in ``$MVN_HOME/conf/logging/`` and fill it with :

.. code-block:: xml

    <?xml version="1.0" encoding="UTF-8" ?>
    <Configuration>
      <Properties>
        <Property name="maven.logging.root.level">INFO</Property>
      </Properties>
      <Appenders>
        <Console name="console" target="SYSTEM_OUT">
          <PatternLayout pattern="[%p] %msg%n%throwable" />
        </Console>
      </Appenders>
      <Loggers>
        <Root level="${sys:maven.logging.root.level}">
          <Appender-ref ref="console"/>
        </Root>
    <!-- <logger name="[USER_MESSAGE]" level="DEBUG"/> -->
      </Loggers>
    </Configuration>

* Remove if exists :

  * In the directory ``$MVN_HOME/lib`` the file ``maven-sl4j-provider-$MVN_VERSION.jar``
  * In the directory ``$MVN_HOME/conf/logging/`` the file ``deletesimpleLogger.properties``
